<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <properties>
        <ws.legacy.lib.version>1.0</ws.legacy.lib.version>
        <sql-maven-plugin.version>1.5</sql-maven-plugin.version>
        <build-helper-maven-plugin.version>1.5</build-helper-maven-plugin.version>
        <copy-configuration.phase>generate-resources</copy-configuration.phase>
        <config.dir>ws</config.dir>
    </properties>

    <parent>
        <groupId>com.sdl.lt.worldserver</groupId>
        <artifactId>worldserver-wsxi</artifactId>
        <version>11.3.1</version>
    </parent>

    <artifactId>ws-web</artifactId>
    <name>ws-web</name>
    <description>User interface implementation</description>
    <packaging>war</packaging>

    <dependencies>
        <dependency>
            <groupId>com.sdl.lt.worldserver</groupId>
            <artifactId>ws-configuration</artifactId>
        </dependency>
        <dependency>
            <groupId>com.sdl.lt.worldserver</groupId>
            <artifactId>ws-api-model</artifactId>
        </dependency>
        <dependency>
            <groupId>com.sdl.lt.worldserver</groupId>
            <artifactId>ws-web-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.sdl.lt.worldserver</groupId>
            <artifactId>ws-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.sdl.lt.worldserver</groupId>
            <artifactId>ws-rest-template</artifactId>
        </dependency>
        <!-- SPRING -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aspects</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- Spring Security -->
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-core</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-config</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-taglibs</artifactId>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
        </dependency>
        <dependency>
            <groupId>com.jayway.jsonpath</groupId>
            <artifactId>json-path</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
        </dependency>

        <!-- validation -->

        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-validator</artifactId>
        </dependency>

        <!--TESTING-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.dbunit</groupId>
            <artifactId>dbunit</artifactId>
        </dependency>
        <dependency>
            <groupId>com.github.springtestdbunit</groupId>
            <artifactId>spring-test-dbunit</artifactId>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
        </dependency>
        <dependency>
            <groupId>com.sdl.wsop</groupId>
            <artifactId>dynamic-context</artifactId>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <build>
        <finalName>ws</finalName>
        <plugins>
            <plugin>
                <groupId>org.apache.tomcat.maven</groupId>
                <artifactId>tomcat7-maven-plugin</artifactId>
                <version>2.2</version>
                <configuration>
                    <port>8090</port>
                    <webapps>
                        <webapp>
                            <contextPath>/ws</contextPath>
                            <groupId>com.sdl.lt.worldserver</groupId>
                            <artifactId>ws-web</artifactId>
                            <version>${project.version}</version>
                            <type>war</type>
                            <asWebapp>true</asWebapp>
                        </webapp>
                    </webapps>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.16</version>
                <configuration>
                    <includes>
                        <include>**/*Test.java</include>
                    </includes>
                </configuration>
            </plugin>

            <!-- configu -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy configuration</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}</outputDirectory>
                            <includes>WS_CONFIG/${config.dir}/**/*.*</includes>
                            <overWriteReleases>false</overWriteReleases>
                            <overWriteSnapshots>true</overWriteSnapshots>
                            <includeArtifactIds>ws-configuration</includeArtifactIds>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-config</id>
                        <phase>prepare-package</phase>
                        <configuration>
                            <target>
                                <copy todir="${project.build.outputDirectory}/config">
                                    <fileset dir="${project.build.directory}/WS_CONFIG/${config.dir}"/>
                                </copy>
                            </target>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                </includes>
                <filtering>true</filtering>

            </resource>
        </resources>
        <testResources>
            <testResource>
                <directory>src/main/webapp/WEB-INF</directory>
            </testResource>
            <testResource>
                <directory>src/testintegration/resources</directory>
                <filtering>true</filtering>
            </testResource>
        </testResources>
    </build>
</project>