--===================================================================================================--
-- This script is Oracle Version of upgrading World Server database from ${version.from} to ${version.to}
-- Please read the WorldServer V${version.to} readme.pdf (SDL WorldServer Version ${version.to} Release Notes) and --
-- BACKUP the DATABASE before running this script.                                                   --
-- Contact SDL Professional Support if you have any questions.                                     --
--===================================================================================================--

SET SERVEROUTPUT ON SIZE 1000000
SET VERIFY OFF
SET PAGESIZE 0
SET LINESIZE 255
SET NEWPAGE NONE
SET NUMWIDTH 12
SET TIMING ON
WHENEVER SQLERROR EXIT FAILURE
WHENEVER OSERROR EXIT FAILURE
SET DEFINE OFF

--======================== WS Database Version Checking ==========================
DECLARE
  lvcValue VARCHAR(256);
  tcount   INTEGER;
  vsqlstmt VARCHAR2(2048);
BEGIN
  -- make sure that WS is installed
  dbms_output.put_line('-->Start Upgrading Database from WS${version.from} to WS${version.to} ...');

  SELECT value
  INTO lvcValue
  FROM configuration
  WHERE upper(name) = upper('internal/current_schema_version');

  IF (lvcValue = '${version.to}')
  THEN
    dbms_output.put_line('Warning: Schema has already been migrated to version: ' || lvcValue);
    dbms_output.put_line('------> Ignore the warning message if you re-run the script!');
  ELSE
    IF (lvcValue != '${version.from}')
    THEN
      dbms_output.put_line('Warning: Schema is not expected version for migration: ' || lvcValue);
      RAISE_APPLICATION_ERROR(-20002,'Schema is not expected version for migration: '||lvcValue);
    END IF;
  END IF;

  EXCEPTION
  WHEN NO_DATA_FOUND THEN
  lvcValue := '';
  WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20003, 'General Error: Error during schema version check: ' || SQLERRM);
END;
/

--- UPGRADE SCRIPTS ---


--- END OF UPGRADE SCRIPTS ---

-- --------------============= Upgrade Schema Version Data ==================================
DECLARE
  tcount INTEGER;
BEGIN
  dbms_output.enable(99000);

  dbms_output.put_line('---->Start Version Upgrade...');
  SELECT count(*)
  INTO tcount
  FROM configuration
  WHERE propertyId = 20
        AND name = 'internal/current_schema_version';

  IF (tcount = 0)
  THEN
    INSERT INTO configuration (propertyid, name, value)
    VALUES (20, 'internal/current_schema_version', '${version.to}');
  ELSE
    UPDATE configuration
    SET value = '${version.to}'
    WHERE propertyId = 20
          AND name = 'internal/current_schema_version';
  END IF;
  COMMIT;

  dbms_output.put_line('---->Schema Version Upgrade to ${version.to}  Completed!');
  dbms_output.put_line('****All Steps in the Main Upgrade Script for Schema Upgrade  Completed!****');
  dbms_output.put_line('****Please check the SDL WorldServer Version ${version.to} Release Notes and run other scripts to complete the Schema Upgrade!****');

  EXCEPTION
  WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20004, 'Schema Version Upgrade  failed for: ' || SQLERRM);
END;
/