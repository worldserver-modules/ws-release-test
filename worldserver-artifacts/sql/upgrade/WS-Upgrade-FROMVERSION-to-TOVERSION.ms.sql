--=======================================================================================================--
-- This script is Microsoft SQL Server Version for upgrading World Server database from ${version.from} to ${version.to}   --
-- Please read the WorldServer V${version.to} readme.pdf (SDL WorldServer Version ${version.to} Release Notes) before  --
-- running this script. Contact SDL Professional Support if you have any questions.                    --
--=======================================================================================================--

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

set NOCOUNT ON
--======================== WS Database Version Checking ==========================
DECLARE @icount int

-- make sure that WS is installed
print '-->Start Upgrading Database from WS${version.from} to WS${version.to} ...'

select @icount=count(*)
from dbo.sysobjects
where name='configuration'
      and type='U'

if(@icount=0)
  begin
    declare @dbname varchar(256)
    select @dbname = db_name()
    raiserror('Cannot find configuration data for current database: %s',16,127, @dbname)
  end
go


-- Check the version to see if this has already been run
DECLARE @value nvarchar(256)

select @value=value
from configuration
where upper(name)=upper('internal/current_schema_version')

print('-->checking WS database version')

if(@value = '${version.from}')
  begin
    print('-->WS database version checking Successful!')
  end
else if(@value = '${version.to}')
  begin
    print('-->WARNING: WS database has already upgraded to ${version.to}!')
    print('-->Please ignore this warning message if you re-run the script!')
  end
else
  begin
    print('-->Error: WS database version checking Failed!')
    raiserror('Database is not expected version for migration. Current version: %s',16,127, @value)
  end
go

--- UPGRADE SCRIPTS ---


--- END OF UPGRADE SCRIPTS ---

--------------============= Upgrade Schema Version Data ==================================
BEGIN

  DECLARE @tcount INT

  print('---->Start Version Upgrade...')

  SELECT @tcount=count(*)
  FROM configuration
  WHERE propertyId = 20
        AND name = 'internal/current_schema_version'

  IF (@tcount=0)
    BEGIN
      insert into configuration (propertyid, name, value)
      VALUES (20, 'internal/current_schema_version', '${version.to}')
    END
  ELSE
    BEGIN
      UPDATE configuration
      SET value = '${version.to}'
      WHERE propertyId = 20
            AND name = 'internal/current_schema_version'
    END

  print('---->Schema Version Upgraded to ${version.to}!')
  print('****All Steps in the Main Upgrade Script for Schema Upgrade  Completed!****')
  print('****Please check the SDL WorldServer Version ${version.to} Release Notes and run other scripts to complete the Schema Upgrade!****')

END
GO
--============================= End of the Script ======================================




