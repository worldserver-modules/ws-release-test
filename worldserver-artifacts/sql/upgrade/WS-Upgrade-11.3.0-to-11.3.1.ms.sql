--=======================================================================================================--
-- This script is Microsoft SQL Server Version for upgrading World Server database from 11.3.0 to 11.3.1   --
-- Please read the WorldServer V11.3.1 readme.pdf (SDL WorldServer Version 11.3.1 Release Notes) before  --
-- running this script. Contact SDL Professional Support if you have any questions.                    --
--=======================================================================================================--

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

set NOCOUNT ON
--======================== WS Database Version Checking ==========================
DECLARE @icount int

-- make sure that WS is installed
print '-->Start Upgrading Database from WS11.3.0 to WS11.3.1 ...'

select @icount=count(*)
from dbo.sysobjects
where name='configuration'
      and type='U'

if(@icount=0)
  begin
    declare @dbname varchar(256)
    select @dbname = db_name()
    raiserror('Cannot find configuration data for current database: %s',16,127, @dbname)
  end
go


-- Check the version to see if this has already been run
DECLARE @value nvarchar(256)

select @value=value
from configuration
where upper(name)=upper('internal/current_schema_version')

print('-->checking WS database version')

if(@value = '11.3.0')
  begin
    print('-->WS database version checking Successful!')
  end
else if(@value = '11.3.1')
  begin
    print('-->WARNING: WS database has already upgraded to 11.3.1!')
    print('-->Please ignore this warning message if you re-run the script!')
  end
else
  begin
    print('-->Error: WS database version checking Failed!')
    raiserror('Database is not expected version for migration. Current version: %s',16,127, @value)
  end
go

--- UPGRADE SCRIPTS ---


--- END OF UPGRADE SCRIPTS ---

--------------============= Upgrade Schema Version Data ==================================
BEGIN

  DECLARE @tcount INT

  print('---->Start Version Upgrade...')

  SELECT @tcount=count(*)
  FROM configuration
  WHERE propertyId = 20
        AND name = 'internal/current_schema_version'

  IF (@tcount=0)
    BEGIN
      insert into configuration (propertyid, name, value)
      VALUES (20, 'internal/current_schema_version', '11.3.1')
    END
  ELSE
    BEGIN
      UPDATE configuration
      SET value = '11.3.1'
      WHERE propertyId = 20
            AND name = 'internal/current_schema_version'
    END

  print('---->Schema Version Upgraded to 11.3.1!')
  print('****All Steps in the Main Upgrade Script for Schema Upgrade  Completed!****')
  print('****Please check the SDL WorldServer Version 11.3.1 Release Notes and run other scripts to complete the Schema Upgrade!****')

END
GO
--============================= End of the Script ======================================




