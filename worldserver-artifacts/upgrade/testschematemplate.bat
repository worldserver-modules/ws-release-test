@echo off
rem -------------------------------------------------------------------------
rem testschema.bat - ${version} Schema Verification Tool for Windows
rem
rem Verifies that WorldServer database is compliant to WorldServer ${version}
rem schema.
rem
rem Pay attention to the Test Run Summary displayed at end of the test run.
rem Any errors or failures encountered during the test run are detailed in this
rem summary.
rem
rem Can run tests at any point after upgrading, but recommended to do so
rem immediately after completing all relevant upgrading steps so we can
rem identify upgrading issues sooner.
rem
rem Can re-run with debug parameter for debugging information if there are failures.
rem
rem Environment Variable Prerequisites:
rem
rem   WS_HOME       Must point at your WorldServer installation.
rem
rem                 Example for a Tomcat installation:
rem                 %WS_HOME% = %TOMCAT_HOME%\webapps\ws-legacy
rem
rem -------------------------------------------------------------------------

java -Djava.library.path="%WS_HOME%\..\..\..\bin" -Xmx512m -cp "%WS_HOME%\WEB-INF\classes;%WS_HOME%\WEB-INF\lib;%WS_HOME%\wstool.jar;%WS_HOME%\..\..\lib\jregistry-1.7.4.jar" com.idiominc.ws.test.upgrade.Schema${version.short}Test %1
