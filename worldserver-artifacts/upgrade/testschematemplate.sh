#!/bin/sh
# -------------------------------------------------------------------------
# testschema.sh - ${version} Schema Verification Tool for UNIX
#
# Verifies that WorldServer database is compliant to WorldServer ${version}
# schema.
#
# Pay attention to the Test Run Summary displayed at end of the test run.
# Any errors or failures encountered during the test run are detailed in this
# summary.
#
# Can run tests at any point after upgrading, but recommended to do so
# immediately after completing all relevant upgrading steps so we can
# identify upgrading issues sooner.
#
# Can re-run with debug parameter for debugging information if there are failures.
#
# 
# Environment Variable Prerequisites:
#
#   com._HOME       Must point at your WorldServer installation.
#
#                 Example for a Tomcat installation:
#                 $WS_HOME = $TOMCAT_HOME/webapps/ws-legacy
#
# -------------------------------------------------------------------------

java -Xmx512m -cp $WS_HOME/WEB-INF/classes:$WS_HOME/wstool.jar com.idiominc.ws.test.upgrade.Schema${version.short}Test $1